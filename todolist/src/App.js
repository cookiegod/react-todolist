import React, { Component } from 'react';
import Todos from './components/Todos';
import './App.css';

class App extends Component {
  state = {
    todos: [
      {
        id: 1,
        title: 'Take out the trash',
        completed: false
      },
      {
        id: 2,
        title: 'Test2',
        completed: true
      }, {
        id: 3,
        title: 'Test3',
        completed: false
      },
    ]
  }
  render() {
    return (
      <div className="App">
        <h1>Index</h1>
        <Todos todos={this.state.todos} />
      </div>
    )
  }
}

export default App;
